#!/bin/sh

newaliases

postconf -ve maillog_file=/dev/stdout
postconf -ve recipient_delimiter=+
postconf -ve virtual_mailbox_domains=gitlab.alpinelinux.org
postconf -ve virtual_transport=lmtp:dovecot:2525

if [ "$POSTFIX_MYHOSTNAME" ]; then
	postconf -ve myhostname="$POSTFIX_MYHOSTNAME"
fi

if [ -f "/etc/postfix/transport" ]; then
	postconf -ve transport_maps=hash:/etc/postfix/transport
	postmap /etc/postfix/transport
fi

exec postfix "$@"
