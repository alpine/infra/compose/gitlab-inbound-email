# GitLab incoming email support #

This docker compose add incoming email support for GitLab.

Postfix will listen on port 25 and transport email over LMTP to Dovecot.
GitLab will then be able to fetch email via mailroom over IMAP.

## Setup ##

### Docker Compose ###
Create a passwd file in the Dovecot config directory like:

`incoming@gitlab.alpinelinux.org:{plain}mysecretpassword`

### GitLab ###

Follow the GitLab incoming email documentation on how to setup GitLab.

https://docs.gitlab.com/ee/administration/incoming_email.html

Make sure GitLab has access to the gitlab_email network.

## Running

`docker-compose up -d`
